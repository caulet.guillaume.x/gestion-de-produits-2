<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('roles',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('password',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('last_name',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('first_name',TextType::class,['attr' => ['class' => 'form-control mb-2']])
            ->add('adress',TextType::class,['attr' => ['class' => 'form-control mb-2']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
